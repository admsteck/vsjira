# About #
VSJira is a Visual Studio Extension that adds tools to the IDE to interact with JIRA servers.

# Install #
[Download from the Visual Studio Gallery.](https://visualstudiogallery.msdn.microsoft.com/32d406da-a2c4-4856-b1d7-c23cb2bc9750)

# Change History #

[See change history page](https://bitbucket.org/farmas/vsjira/src/master/ChangeHistory.md)

# Features #

### JIRA Issues Tool Window ###
Select from your favorite JIRA filters and displays the issues on a grid.

![MenuCommand1](https://bytebucket.org/farmas/vsjira/raw/master/Media/MenuCommand1.png)

![ConnectDialog1](https://bytebucket.org/farmas/vsjira/raw/master/Media/ConnectDialog1.png)

![JiraIssuesToolWindow1](https://bytebucket.org/farmas/vsjira/raw/master/Media/JiraIssuesToolWindow1.png)

### JIRA Issue Form Tool Window ###
View and edit main fields of JIRA Issues.

![JiraIssueToolWindow1](https://bytebucket.org/farmas/vsjira/raw/master/Media/JiraIssueFormToolWindow1.png)

### Options Page ###
Save settings for multiple JIRA servers in Visual Studio options page.

![OptionPage1](https://bytebucket.org/farmas/vsjira/raw/master/Media/OptionPage1.png)


# License #

This project is licensed under [BSD](https://bitbucket.org/farmas/vsjira/raw/master/VSJira.Package/LICENSE.txt).

# Roadmap #
This is a personal project and I am open to take requests/suggestions to improve or add features that are useful to the community. Please create issues with your requests so we can disuss them.