﻿using EnvDTE;
using Microsoft.VisualStudio.Shell;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using VSJira.Core;

namespace VSJira.Package
{
    public class VisualStudioServices : IVisualStudioServices
    {
        private readonly DTE _env;
        private readonly VSJiraPackage _package;

        public VisualStudioServices(DTE environment, VSJiraPackage package)
        {
            this._env = environment;
            this._package = package;
        }

        public void ShowOptionsPage()
        {
            this._package.ShowOptionPage(typeof(JiraOptionsDialogPage));
        }

        public IJiraOptions GetConfigurationOptions()
        {
            var result = new JiraOptions();
            var properties = this._env.get_Properties(JiraOptionsDialogPage.CategoryName, JiraOptionsDialogPage.PageName);

            if (properties != null)
            {
                Object[] serverObjects;
                if (TryGetProperty(properties, "JiraServers", out serverObjects))
                {
                    result.JiraServers = serverObjects.Cast<JiraServerInfo>().ToArray();

                }

                int maxIssuesPerRequest;
                if (TryGetProperty(properties, "MaxIssuesPerRequest", out maxIssuesPerRequest))
                {
                    result.MaxIssuesPerRequest = maxIssuesPerRequest;
                }
            }

            return result;
        }

        private bool TryGetProperty<T>(Properties properties, string propertyName, out T value)
        {
            value = (T)properties.Item(propertyName).Value;

            return value != null;
        }

        public void ShowJiraIssueToolWindow(Core.UI.JiraIssueViewModel viewModel, VSJiraServices services)
        {
            this._package.ShowJiraIssueToolWindow(viewModel, services);
        }
    }
}
