﻿using EnvDTE;
using Microsoft.VisualStudio.Shell;
using Microsoft.VisualStudio.Shell.Interop;
using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Runtime.InteropServices;
using System.Windows;
using VSJira.Core;
using VSJira.Core.UI;

namespace VSJira.Package
{
    [Guid(GuidList.guidJiraIssueToolWindow)]
    public class JiraIssueToolWindow : ToolWindowPane
    {
        private readonly JiraIssueUserControlViewModel _viewModel;

        public JiraIssueToolWindow() :
            base(null)
        {
            this.BitmapResourceID = 301;
            this.BitmapIndex = 1;

            _viewModel = new JiraIssueUserControlViewModel();
            base.Content = new JiraIssueUserControl(_viewModel);
        }

        public void Initialize(JiraIssueViewModel issue, VSJiraServices services)
        {
            base.Caption = issue.Key;
            _viewModel.Issue = issue;
            _viewModel.Services = services;
        }
    }
}
