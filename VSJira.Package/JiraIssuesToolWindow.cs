﻿using EnvDTE;
using Microsoft.VisualStudio.Shell;
using Microsoft.VisualStudio.Shell.Interop;
using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Runtime.InteropServices;
using System.Windows;
using VSJira.Core;
using VSJira.Core.UI;

namespace VSJira.Package
{
    /// <summary>
    /// This class implements the tool window exposed by this package and hosts a user control.
    ///
    /// In Visual Studio tool windows are composed of a frame (implemented by the shell) and a pane,
    /// usually implemented by the package implementer.
    ///
    /// This class derives from the ToolWindowPane class provided from the MPF in order to use its
    /// implementation of the IVsUIElementPane interface.
    /// </summary>
    [Guid(GuidList.guidJiraIssuesToolWindow)]
    public class JiraIssuesToolWindow : ToolWindowPane
    {
        /// <summary>
        /// Standard constructor for the tool window.
        /// </summary>
        public JiraIssuesToolWindow() :
            base(null)
        {
            // Set the window title reading it from the resources.
            this.Caption = Resources.ToolWindowTitle;
            // Set the image that will appear on the tab of the window frame
            // when docked with an other window
            // The resource ID correspond to the one defined in the resx file
            // while the Index is the offset in the bitmap strip. Each image in
            // the strip being 16x16.
            this.BitmapResourceID = 301;
            this.BitmapIndex = 1;
        }

        protected override void Initialize()
        {
            DTE env = (DTE)GetService(typeof(DTE));

            var services = new VSJiraServices()
            {
                JiraFactory = new JiraFactory(),
                WindowFactory = new WindowFactory(),
                VisualStudioServices = new VisualStudioServices(env, (VSJiraPackage)this.Package)
            };
            var viewModel = new JiraIssuesUserControlViewModel(services);

            base.Content = new JiraIssuesUserControl(viewModel);

            base.Initialize();
        }
    }
}
