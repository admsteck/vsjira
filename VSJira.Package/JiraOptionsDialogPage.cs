﻿using Microsoft.VisualStudio.Settings;
using Microsoft.VisualStudio.Shell;
using Microsoft.VisualStudio.Shell.Settings;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;
using VSJira.Core;

namespace VSJira.Package
{
    [ClassInterface(ClassInterfaceType.AutoDual)]
    [CLSCompliant(false), ComVisible(true)]
    public class JiraOptionsDialogPage : DialogPage, IJiraOptions
    {
        public const string CategoryName = "Atlassian";
        public const string PageName = "Jira";

        private const int DefaultMaxIssuesPerRequest = 100;
        private const string SettingsCollectionName = "VSJira";
        private const string SettingsPropertyName = "JiraSettings";

        [DisplayName("JIRA Servers")]
        [Description("The list of JIRA servers registered.")]
        public JiraServerInfo[] JiraServers { get; set; }

        [DisplayName("Issues Per Request")]
        [Description("Maximum number of issues to retrieve per request.")]
        public int MaxIssuesPerRequest { get; set; }

        public JiraOptionsDialogPage()
        {
            this.MaxIssuesPerRequest = DefaultMaxIssuesPerRequest;
        }

        public override void SaveSettingsToStorage()
        {
            base.SaveSettingsToStorage();

            var settingsManager = new ShellSettingsManager(ServiceProvider.GlobalProvider);
            var userSettingsStore = settingsManager.GetWritableSettingsStore(SettingsScope.UserSettings);

            if (!userSettingsStore.CollectionExists(SettingsCollectionName))
            {
                userSettingsStore.CreateCollection(SettingsCollectionName);
            }

            var settingString = JsonConvert.SerializeObject(new JiraOptions()
            {
                MaxIssuesPerRequest = this.MaxIssuesPerRequest,
                JiraServers = this.JiraServers
            });

            userSettingsStore.SetString(
                SettingsCollectionName,
                SettingsPropertyName,
                settingString);
        }

        public override void LoadSettingsFromStorage()
        {
            base.LoadSettingsFromStorage();

            var settingsManager = new ShellSettingsManager(ServiceProvider.GlobalProvider);
            var userSettingsStore = settingsManager.GetWritableSettingsStore(SettingsScope.UserSettings);

            if (userSettingsStore.PropertyExists(SettingsCollectionName, SettingsPropertyName))
            {
                var settingString = userSettingsStore.GetString(SettingsCollectionName, SettingsPropertyName);
                var jiraOptions = JsonConvert.DeserializeObject<JiraOptions>(settingString);
                this.JiraServers = jiraOptions.JiraServers;
                this.MaxIssuesPerRequest = jiraOptions.MaxIssuesPerRequest;
            }
        }
    }
}
