﻿using Atlassian.Jira;
using Atlassian.Jira.Remote;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace VSJira.Core
{
    public class TestJiraClient : IJiraClient
    {
        private readonly int _totalIssues;
        private string[] _jiraFilters = new string[2] { "My Open Issues", "Assigned To Me" };

        #region Ipsum Lorem
        private const string IpsumLorem = @"Lorem ipsum dolor sit amet, consectetur adipiscing elit. Quisque finibus odio vel feugiat lacinia. Nulla eu magna sit amet arcu eleifend facilisis. Cras vel nulla bibendum, tristique mi id, suscipit leo. Phasellus magna metus, aliquet id felis id, rhoncus gravida orci. Cras eu dui vitae metus rhoncus aliquet eget ac magna. Integer rhoncus, libero ac ornare venenatis, sapien magna sodales sapien, eget rutrum nulla ante at quam. Cras ac laoreet purus. Mauris vulputate maximus ex et pulvinar. Aliquam quis blandit libero, id cursus nibh. In eu porta sem, nec dignissim nisi.

Suspendisse vel ante gravida, sagittis tortor quis, pellentesque dui. Proin placerat mi ac congue convallis. Duis et arcu ligula. Sed vel auctor neque. Donec in nunc eu diam elementum dignissim eu nec mauris. Nulla rutrum mi ante, a ullamcorper turpis congue ullamcorper. Suspendisse ac vehicula neque, ac aliquam augue. Phasellus condimentum dapibus nulla, convallis pharetra ex. Integer lorem quam, tristique ac sodales et, eleifend id dolor. In semper turpis justo, fringilla semper lorem finibus vitae. Curabitur aliquet tellus vitae lectus posuere, vitae posuere ligula consectetur. Pellentesque sed ante erat.

Integer dapibus est ut ultrices dictum. Proin vitae est mattis, volutpat orci id, hendrerit risus. Nulla ac urna lobortis, ornare quam in, imperdiet ante. Aenean facilisis rutrum libero, at placerat mi. Praesent maximus convallis pulvinar. Sed sodales non ipsum quis convallis. Curabitur posuere molestie est, sed bibendum diam tincidunt eu. Praesent vel quam orci. Nunc fringilla ipsum tempus, gravida arcu at, vehicula lectus. Mauris sollicitudin erat a tincidunt fringilla. Aenean in ex cursus, pretium ligula semper, fringilla libero.";
        #endregion

        private IEnumerable<RemoteIssue> _remoteIssues;

        public TestJiraClient(int totalIssues)
        {
            this._totalIssues = totalIssues;
            var random = new Random();
            _remoteIssues = Enumerable.Range(0, _totalIssues)
                .Select(i => new RemoteIssue()
                {
                    id = String.Format("{0}", i),
                    assignee = "farmas",
                    created = DateTime.Now,
                    key = String.Format("TST-{0}", i),
                    priority = random.Next(0, 2).ToString(),
                    status = random.Next(0, 2).ToString(),
                    summary = String.Format("My Summary {0}", i),
                    type = random.Next(0, 2).ToString(),
                    description = String.Format("{0}: {1}", i, IpsumLorem)
                });
        }

        private Task<IEnumerable<T>> GetEmptyEnumerableTask<T>()
        {
            var taskSource = new TaskCompletionSource<IEnumerable<T>>();
            taskSource.SetResult(Enumerable.Empty<T>());
            return taskSource.Task;
        }

        public Jira Jira { get; set; }

        public Task<IEnumerable<IssueResolution>> GetIssueResolutionsAsync(CancellationToken token)
        {
            return GetEmptyEnumerableTask<IssueResolution>();
        }

        public Task<IEnumerable<CustomField>> GetCustomFieldsAsync(CancellationToken token)
        {
            return GetEmptyEnumerableTask<CustomField>();
        }

        public Task<IEnumerable<IssuePriority>> GetIssuePrioritiesAsync(CancellationToken token)
        {
            return GetEmptyEnumerableTask<IssuePriority>();
        }

        public Task<IEnumerable<IssueStatus>> GetIssueStatusesAsync(CancellationToken token)
        {
            return GetEmptyEnumerableTask<IssueStatus>();
        }

        public Task<IEnumerable<IssueType>> GetIssueTypesAsync(CancellationToken token)
        {
            return GetEmptyEnumerableTask<IssueType>();
        }

        public Task<IEnumerable<JiraFilter>> GetFavouriteFiltersAsync(CancellationToken token)
        {
            var taskSource = new TaskCompletionSource<IEnumerable<Atlassian.Jira.JiraFilter>>();
            taskSource.SetResult(Enumerable.Range(0, 2).Select(i =>
                new JiraFilter()
                {
                    Id = i.ToString(),
                    Name = _jiraFilters[i],
                    Jql = _jiraFilters[i]
                }));

            return taskSource.Task;
        }

        public Task<IEnumerable<Issue>> GetIssuesFromJqlAsync(string jql, int? maxIssues = null, int startAt = 0)
        {
            return GetIssuesFromJqlAsync(jql, maxIssues, startAt, CancellationToken.None);
        }

        public Task<IEnumerable<Issue>> GetIssuesFromJqlAsync(string jql, int? maxIssues, int startAt, CancellationToken token)
        {
            var taskSource = new TaskCompletionSource<IEnumerable<Issue>>();
            var random = new Random();
            var result = new TestPagedIssueQueryResult()
            {
                ItemsPerPage = maxIssues ?? 100,
                StartAt = startAt,
                TotalItems = _totalIssues
            };

            var issues = _remoteIssues.Select(i => i.ToLocal(this.Jira)).ToList();
            SortIssuesList(issues, jql);

            result.Issues = issues.Skip(startAt).Take(maxIssues ?? 100);
            taskSource.SetResult(result);

            return taskSource.Task;
        }

        private void SortIssuesList(List<Issue> issues, string jql)
        {
            if (jql.IndexOf("order by", StringComparison.OrdinalIgnoreCase) < 0)
            {
                return;
            }

            var sortArray = jql.Split(' ').Reverse().Take(2).ToArray();
            var sortDirection = sortArray[0];
            var sortField = sortArray[1];

            issues.Sort((x, y) =>
            {
                var property = typeof(Issue).GetProperty(sortField);
                var valX = property.GetValue(x).ToString();
                var valY = property.GetValue(y).ToString();
                var comparison = valX.CompareTo(valY);

                if (sortDirection == "desc")
                {
                    comparison *= -1;
                }

                return comparison;
            });
        }

        public string Url
        {
            get { return "http://testjira"; }
        }

        public Task<Issue> GetIssueAsync(string issueKey, CancellationToken token)
        {
            throw new NotImplementedException();
        }

        public Task<Issue> UpdateIssueAsync(Issue issue, CancellationToken token)
        {
            var taskSource = new TaskCompletionSource<Issue>();
            taskSource.SetResult(issue);
            return taskSource.Task;
        }

        public RestSharp.IRestResponse ExecuteRequest(RestSharp.IRestRequest request)
        {
            throw new NotImplementedException();
        }

        public Newtonsoft.Json.Linq.JToken ExecuteRequest(RestSharp.Method method, string resource, object requestBody = null)
        {
            throw new NotImplementedException();
        }

        public Task<Newtonsoft.Json.Linq.JToken> ExecuteRequestAsync(RestSharp.Method method, string resource, object requestBody = null)
        {
            throw new NotImplementedException();
        }

        public Task<Newtonsoft.Json.Linq.JToken> ExecuteRequestAsync(RestSharp.Method method, string resource, object requestBody, System.Threading.CancellationToken token)
        {
            throw new NotImplementedException();
        }

        public T ExecuteRequest<T>(RestSharp.Method method, string resource, object requestBody = null)
        {
            throw new NotImplementedException();
        }

        public Task<T> ExecuteRequestAsync<T>(RestSharp.Method method, string resource, object requestBody = null)
        {
            throw new NotImplementedException();
        }

        public Task<T> ExecuteRequestAsync<T>(RestSharp.Method method, string resource, object requestBody, System.Threading.CancellationToken token)
        {
            throw new NotImplementedException();
        }

        public Atlassian.Jira.IssueTimeTrackingData GetTimeTrackingData(string issueKey)
        {
            throw new NotImplementedException();
        }

        public string Login(string username, string password)
        {
            throw new NotImplementedException();
        }

        public void AddLabels(string token, RemoteIssue issue, string[] labels)
        {
            throw new NotImplementedException();
        }

        public RemoteIssue[] GetIssuesFromJqlSearch(string token, string jqlSearch, int maxResults, int startAt = 0)
        {
            throw new NotImplementedException();
        }

        public RemoteIssue CreateIssue(string token, RemoteIssue newIssue)
        {
            throw new NotImplementedException();
        }

        public RemoteIssue CreateIssueWithParent(string token, RemoteIssue newIssue, string parentIssueKey)
        {
            throw new NotImplementedException();
        }

        public RemoteIssue UpdateIssue(string token, RemoteIssue issue, RemoteFieldValue[] fields)
        {
            throw new NotImplementedException();
        }

        public RemoteAttachment[] GetAttachmentsFromIssue(string token, string key)
        {
            throw new NotImplementedException();
        }

        public bool AddBase64EncodedAttachmentsToIssue(string token, string key, string[] fileNames, string[] base64EncodedAttachmentData)
        {
            throw new NotImplementedException();
        }

        public RemoteComment[] GetCommentsFromIssue(string token, string key)
        {
            throw new NotImplementedException();
        }

        public void AddComment(string token, string key, RemoteComment comment)
        {
            throw new NotImplementedException();
        }

        public RemoteIssueType[] GetIssueTypes(string token, string projectId)
        {
            throw new NotImplementedException();
        }

        public RemotePriority[] GetPriorities(string token)
        {
            throw new NotImplementedException();
        }

        public RemoteResolution[] GetResolutions(string token)
        {
            throw new NotImplementedException();
        }

        public RemoteStatus[] GetStatuses(string token)
        {
            throw new NotImplementedException();
        }

        public RemoteVersion[] GetVersions(string token, string projectKey)
        {
            throw new NotImplementedException();
        }

        public RemoteComponent[] GetComponents(string token, string projectKey)
        {
            throw new NotImplementedException();
        }

        public RemoteField[] GetCustomFields(string token)
        {
            throw new NotImplementedException();
        }

        public RemoteField[] GetFieldsForEdit(string token, string key)
        {
            throw new NotImplementedException();
        }

        public RemoteWorklog AddWorklogAndAutoAdjustRemainingEstimate(string token, string key, RemoteWorklog worklog)
        {
            throw new NotImplementedException();
        }

        public RemoteWorklog AddWorklogAndRetainRemainingEstimate(string token, string key, RemoteWorklog worklog)
        {
            throw new NotImplementedException();
        }

        public RemoteWorklog AddWorklogWithNewRemainingEstimate(string token, string key, RemoteWorklog worklog, string newRemainingEstimate)
        {
            throw new NotImplementedException();
        }

        public RemoteWorklog[] GetWorkLogs(string token, string key)
        {
            throw new NotImplementedException();
        }

        public RemoteProject[] GetProjects(string token)
        {
            throw new NotImplementedException();
        }

        public RemoteFilter[] GetFavouriteFilters(string token)
        {
            throw new NotImplementedException();
        }

        public RemoteIssue[] GetIssuesFromFilterWithLimit(string token, string filterId, int offset, int maxResults)
        {
            throw new NotImplementedException();
        }

        public RemoteNamedObject[] GetAvailableActions(string token, string issueKey)
        {
            throw new NotImplementedException();
        }

        public RemoteIssue ProgressWorkflowAction(string token, RemoteIssue remoteIssue, string actionId, RemoteFieldValue[] remoteFieldValues)
        {
            throw new NotImplementedException();
        }

        public void DeleteWorklogAndAutoAdjustRemainingEstimate(string token, string issueKey, string worklogId)
        {
            throw new NotImplementedException();
        }

        public void DeleteWorklogAndRetainRemainingEstimate(string token, string issueKey, string worklogId)
        {
            throw new NotImplementedException();
        }

        public void DeleteWorklogWithNewRemainingEstimate(string token, string issueKey, string worklogId, string newRemainingEstimate)
        {
            throw new NotImplementedException();
        }

        public void DeleteIssue(string token, string issueKey)
        {
            throw new NotImplementedException();
        }

        public void AddActorsToProjectRole(string in0, string[] in1, RemoteProjectRole in2, RemoteProject in3, string in4)
        {
            throw new NotImplementedException();
        }

        public void AddDefaultActorsToProjectRole(string in0, string[] in1, RemoteProjectRole in2, string in3)
        {
            throw new NotImplementedException();
        }

        public RemotePermissionScheme AddPermissionTo(string in0, RemotePermissionScheme in1, RemotePermission in2, RemoteEntity in3)
        {
            throw new NotImplementedException();
        }

        public void AddUserToGroup(string in0, RemoteGroup in1, RemoteUser in2)
        {
            throw new NotImplementedException();
        }

        public RemoteVersion AddVersion(string in0, string in1, RemoteVersion in2)
        {
            throw new NotImplementedException();
        }

        public void ArchiveVersion(string in0, string in1, string in2, bool in3)
        {
            throw new NotImplementedException();
        }

        public RemoteGroup CreateGroup(string in0, string in1, RemoteUser in2)
        {
            throw new NotImplementedException();
        }

        public RemoteIssue CreateIssueWithSecurityLevel(string in0, RemoteIssue in1, long in2)
        {
            throw new NotImplementedException();
        }

        public RemotePermissionScheme CreatePermissionScheme(string in0, string in1, string in2)
        {
            throw new NotImplementedException();
        }

        public RemoteProject CreateProject(string in0, string in1, string in2, string in3, string in4, string in5, RemotePermissionScheme in6, RemoteScheme in7, RemoteScheme in8)
        {
            throw new NotImplementedException();
        }

        public RemoteProject CreateProjectFromObject(string in0, RemoteProject in1)
        {
            throw new NotImplementedException();
        }

        public RemoteProjectRole CreateProjectRole(string in0, RemoteProjectRole in1)
        {
            throw new NotImplementedException();
        }

        public RemoteUser CreateUser(string in0, string in1, string in2, string in3, string in4)
        {
            throw new NotImplementedException();
        }

        public void DeleteGroup(string in0, string in1, string in2)
        {
            throw new NotImplementedException();
        }

        public RemotePermissionScheme DeletePermissionFrom(string in0, RemotePermissionScheme in1, RemotePermission in2, RemoteEntity in3)
        {
            throw new NotImplementedException();
        }

        public void DeletePermissionScheme(string in0, string in1)
        {
            throw new NotImplementedException();
        }

        public void DeleteProject(string in0, string in1)
        {
            throw new NotImplementedException();
        }

        public void DeleteProjectAvatar(string in0, long in1)
        {
            throw new NotImplementedException();
        }

        public void DeleteProjectRole(string in0, RemoteProjectRole in1, bool in2)
        {
            throw new NotImplementedException();
        }

        public void DeleteUser(string in0, string in1)
        {
            throw new NotImplementedException();
        }

        public RemoteComment EditComment(string in0, RemoteComment in1)
        {
            throw new NotImplementedException();
        }

        public RemotePermission[] GetAllPermissions(string in0)
        {
            throw new NotImplementedException();
        }

        public RemoteScheme[] GetAssociatedNotificationSchemes(string in0, RemoteProjectRole in1)
        {
            throw new NotImplementedException();
        }

        public RemoteScheme[] GetAssociatedPermissionSchemes(string in0, RemoteProjectRole in1)
        {
            throw new NotImplementedException();
        }

        public RemoteComment GetComment(string in0, long in1)
        {
            throw new NotImplementedException();
        }

        public RemoteComment[] GetComments(string in0, string in1)
        {
            throw new NotImplementedException();
        }

        public RemoteConfiguration GetConfiguration(string in0)
        {
            throw new NotImplementedException();
        }

        public RemoteRoleActors GetDefaultRoleActors(string in0, RemoteProjectRole in1)
        {
            throw new NotImplementedException();
        }

        public RemoteField[] GetFieldsForAction(string in0, string in1, string in2)
        {
            throw new NotImplementedException();
        }

        public RemoteGroup GetGroup(string in0, string in1)
        {
            throw new NotImplementedException();
        }

        public RemoteIssue GetIssue(string in0, string in1)
        {
            throw new NotImplementedException();
        }

        public RemoteIssue GetIssueById(string in0, string in1)
        {
            throw new NotImplementedException();
        }

        public long GetIssueCountForFilter(string in0, string in1)
        {
            throw new NotImplementedException();
        }

        public RemoteIssue[] GetIssuesFromTextSearchWithLimit(string in0, string in1, int in2, int in3)
        {
            throw new NotImplementedException();
        }

        public RemoteIssue[] GetIssuesFromTextSearchWithProject(string in0, string[] in1, string in2, int in3)
        {
            throw new NotImplementedException();
        }

        public RemoteIssueType[] GetIssueTypes(string in0)
        {
            throw new NotImplementedException();
        }

        public RemoteIssueType[] GetIssueTypesForProject(string in0, string in1)
        {
            throw new NotImplementedException();
        }

        public RemoteScheme[] GetNotificationSchemes(string in0)
        {
            throw new NotImplementedException();
        }

        public RemotePermissionScheme[] GetPermissionSchemes(string in0)
        {
            throw new NotImplementedException();
        }

        public RemoteAvatar GetProjectAvatar(string in0, string in1)
        {
            throw new NotImplementedException();
        }

        public RemoteAvatar[] GetProjectAvatars(string in0, string in1, bool in2)
        {
            throw new NotImplementedException();
        }

        public RemoteProject GetProjectById(string in0, long in1)
        {
            throw new NotImplementedException();
        }

        public RemoteProject GetProjectByKey(string in0, string in1)
        {
            throw new NotImplementedException();
        }

        public RemoteProjectRole GetProjectRole(string in0, long in1)
        {
            throw new NotImplementedException();
        }

        public RemoteProjectRoleActors GetProjectRoleActors(string in0, RemoteProjectRole in1, RemoteProject in2)
        {
            throw new NotImplementedException();
        }

        public RemoteProjectRole[] GetProjectRoles(string in0)
        {
            throw new NotImplementedException();
        }

        public RemoteProject[] GetProjectsNoSchemes(string in0)
        {
            throw new NotImplementedException();
        }

        public RemoteProject GetProjectWithSchemesById(string in0, long in1)
        {
            throw new NotImplementedException();
        }

        public DateTime GetResolutionDateById(string in0, long in1)
        {
            throw new NotImplementedException();
        }

        public DateTime GetResolutionDateByKey(string in0, string in1)
        {
            throw new NotImplementedException();
        }

        public RemoteSecurityLevel GetSecurityLevel(string in0, string in1)
        {
            throw new NotImplementedException();
        }

        public RemoteSecurityLevel[] GetSecurityLevels(string in0, string in1)
        {
            throw new NotImplementedException();
        }

        public RemoteScheme[] GetSecuritySchemes(string in0)
        {
            throw new NotImplementedException();
        }

        public RemoteServerInfo GetServerInfo(string in0)
        {
            throw new NotImplementedException();
        }

        public RemoteIssueType[] GetSubTaskIssueTypes(string in0)
        {
            throw new NotImplementedException();
        }

        public RemoteIssueType[] GetSubTaskIssueTypesForProject(string in0, string in1)
        {
            throw new NotImplementedException();
        }

        public RemoteUser GetUser(string in0, string in1)
        {
            throw new NotImplementedException();
        }

        public RemoteWorklog[] GetWorklogs(string in0, string in1)
        {
            throw new NotImplementedException();
        }

        public bool HasPermissionToCreateWorklog(string in0, string in1)
        {
            throw new NotImplementedException();
        }

        public bool HasPermissionToDeleteWorklog(string in0, string in1)
        {
            throw new NotImplementedException();
        }

        public bool HasPermissionToEditComment(string in0, RemoteComment in1)
        {
            throw new NotImplementedException();
        }

        public bool HasPermissionToUpdateWorklog(string in0, string in1)
        {
            throw new NotImplementedException();
        }

        public bool IsProjectRoleNameUnique(string in0, string in1)
        {
            throw new NotImplementedException();
        }

        public bool Logout(string in0)
        {
            throw new NotImplementedException();
        }

        public void RefreshCustomFields(string in0)
        {
            throw new NotImplementedException();
        }

        public void ReleaseVersion(string in0, string in1, RemoteVersion in2)
        {
            throw new NotImplementedException();
        }

        public void RemoveActorsFromProjectRole(string in0, string[] in1, RemoteProjectRole in2, RemoteProject in3, string in4)
        {
            throw new NotImplementedException();
        }

        public void RemoveAllRoleActorsByNameAndType(string in0, string in1, string in2)
        {
            throw new NotImplementedException();
        }

        public void RemoveAllRoleActorsByProject(string in0, RemoteProject in1)
        {
            throw new NotImplementedException();
        }

        public void RemoveDefaultActorsFromProjectRole(string in0, string[] in1, RemoteProjectRole in2, string in3)
        {
            throw new NotImplementedException();
        }

        public void RemoveUserFromGroup(string in0, RemoteGroup in1, RemoteUser in2)
        {
            throw new NotImplementedException();
        }

        public void SetNewProjectAvatar(string in0, string in1, string in2, string in3)
        {
            throw new NotImplementedException();
        }

        public void SetProjectAvatar(string in0, string in1, long in2)
        {
            throw new NotImplementedException();
        }

        public RemoteGroup UpdateGroup(string in0, RemoteGroup in1)
        {
            throw new NotImplementedException();
        }

        public RemoteProject UpdateProject(string in0, RemoteProject in1)
        {
            throw new NotImplementedException();
        }

        public void UpdateProjectRole(string in0, RemoteProjectRole in1)
        {
            throw new NotImplementedException();
        }

        public void UpdateWorklogAndAutoAdjustRemainingEstimate(string in0, RemoteWorklog in1)
        {
            throw new NotImplementedException();
        }

        public void UpdateWorklogAndRetainRemainingEstimate(string in0, RemoteWorklog in1)
        {
            throw new NotImplementedException();
        }

        public void UpdateWorklogWithNewRemainingEstimate(string in0, RemoteWorklog in1, string in2)
        {
            throw new NotImplementedException();
        }

    }
}
