﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace VSJira.Core
{
    public class JiraOptions : IJiraOptions
    {
        public JiraServerInfo[] JiraServers { get; set; }
        public int MaxIssuesPerRequest { get; set; }

        public JiraOptions()
        {
            this.JiraServers = new JiraServerInfo[0];
        }
    }
}
