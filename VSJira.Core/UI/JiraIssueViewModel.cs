﻿using Atlassian.Jira;
using Atlassian.Jira.Remote;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;

namespace VSJira.Core.UI
{
    public class JiraIssueViewModel : ViewModelBase
    {
        private readonly VSJiraServices _services;
        private Issue _issue;

        public DelegateCommand OpenIssueCommand { get; private set; }
        public DelegateCommand CopyIssueKeyCommand { get; private set; }
        public DelegateCommand CopyIssueSummaryCommand { get; private set; }
        public DelegateCommand OpenIssueToolWindowCommand { get; private set; }

        public JiraIssueViewModel(Issue issue, VSJiraServices services)
        {
            this._services = services;
            this._issue = issue;
            this.LoadEditableIssueFields(issue);

            this.OpenIssueCommand = new DelegateCommand(() => this.OpenIssue());
            this.CopyIssueKeyCommand = new DelegateCommand(() => this.CopyIssueKey());
            this.CopyIssueSummaryCommand = new DelegateCommand(() => this.CopyIssueSummary());
            this.OpenIssueToolWindowCommand = new DelegateCommand(() => this.OpenIssueToolWindow());
        }

        #region Issue Fields
        private string _summary;
        private string _description;
        private string _type;
        private string _priority;
        private string _environment;
        private string _assignee;
        private string _reporter;
        private DateTime? _dueDate;

        public int Id
        {
            get
            {
                int id = 0;

                if (this._issue.JiraIdentifier != null)
                {
                    int.TryParse(this._issue.JiraIdentifier, out id);
                }

                return id;
            }
        }

        public string Key
        {
            get
            {
                return this._issue.Key != null ? this._issue.Key.Value : "N/A";
            }
        }

        public string Summary
        {
            get
            {
                return _summary;
            }
            set
            {
                _summary = value;
                OnPropertyChanged("Summary");
            }
        }

        public string Description
        {
            get
            {
                return _description;
            }
            set
            {
                _description = value;
                OnPropertyChanged("Description");
            }
        }

        public string Type
        {
            get
            {
                return _type;
            }
            set
            {
                _type = value;
                OnPropertyChanged("Type");
            }
        }

        public string Priority
        {
            get
            {
                return _priority;
            }
            set
            {
                _priority = value;
                OnPropertyChanged("Priority");
            }
        }

        public string Status
        {
            get
            {
                return this._issue.Status != null ? this._issue.Status.Name : null;
            }
        }

        public string Resolution
        {
            get
            {
                return this._issue.Resolution != null ? this._issue.Resolution.Name : null;
            }
        }

        public string Environment
        {
            get
            {
                return _environment;
            }
            set
            {
                _environment = value;
                OnPropertyChanged("Environment");
            }
        }

        public string Assignee
        {
            get
            {
                return _assignee;
            }
            set
            {
                _assignee = value;
                OnPropertyChanged("Assignee");
            }
        }

        public string Reporter
        {
            get
            {
                return _reporter;
            }
            set
            {
                _reporter = value;
            }
        }

        public DateTime? Created
        {
            get
            {
                return this._issue.Created;
            }
        }

        public DateTime? ResolutionDate
        {
            get
            {
                return this._issue.ResolutionDate;
            }
        }

        public DateTime? Updated
        {
            get
            {
                return this._issue.Updated;
            }
        }

        public DateTime? DueDate
        {
            get
            {
                return _dueDate;
            }
            set
            {
                _dueDate = value;
                OnPropertyChanged("DueDate");
            }
        }
        #endregion

        internal Jira Jira
        {
            get
            {
                return this._issue.Jira;
            }
        }

        public JiraIssueViewModel Clone()
        {
            return new JiraIssueViewModel(this._issue, this._services);
        }

        public Task RefreshIssueAsync(CancellationToken token)
        {
            return this._issue.Jira.RestClient.GetIssueAsync(this._issue.Key.Value, token).ContinueWith(task =>
            {
                this._issue = task.Result;
                LoadEditableIssueFields(this._issue);
                OnAllPropertiesChanged();
            });
        }

        public Task UpdateIssueAsync(JiraIssueViewModel editedViewModel, CancellationToken token)
        {
            // clone the issue and change all its editable fields.
            var clonedIssue = this._issue.ToRemote().ToLocal(this.Jira);
            clonedIssue.Summary = editedViewModel.Summary;
            clonedIssue.Description = editedViewModel.Description;
            clonedIssue.Type = editedViewModel.Type;
            clonedIssue.Priority = editedViewModel.Priority;
            clonedIssue.Environment = editedViewModel.Environment;
            clonedIssue.Assignee = editedViewModel.Assignee;
            clonedIssue.Reporter = editedViewModel.Reporter;
            clonedIssue.DueDate = editedViewModel.DueDate;

            // Attempt the issue update, change the inner fields if successfull.
            return this._issue.Jira.RestClient.UpdateIssueAsync(clonedIssue, token).ContinueWith(task =>
            {
                this._issue = task.Result;
                LoadEditableIssueFields(this._issue);
                OnAllPropertiesChanged();
            });
        }

        private void LoadEditableIssueFields(Issue issue)
        {
            this.Summary = issue.Summary;
            this.Description = issue.Description;
            this.Type = this._issue.Type != null ? this._issue.Type.Name : null;
            this.Priority = this._issue.Priority != null ? this._issue.Priority.Name : null;
            this.Environment = this._issue.Environment;
            this.Assignee = this._issue.Assignee;
            this.Reporter = this._issue.Reporter;
            this.DueDate = this._issue.DueDate;
        }

        private void OnAllPropertiesChanged()
        {
            OnPropertyChanged("Id");
            OnPropertyChanged("Key");
            OnPropertyChanged("Summary");
            OnPropertyChanged("Description");
            OnPropertyChanged("Type");
            OnPropertyChanged("Priority");
            OnPropertyChanged("Status");
            OnPropertyChanged("Resolution");
            OnPropertyChanged("Environment");
            OnPropertyChanged("Assignee");
            OnPropertyChanged("Created");
            OnPropertyChanged("Reporter");
            OnPropertyChanged("ResolutionDate");
            OnPropertyChanged("Updated");
            OnPropertyChanged("DueDate");
        }

        private void CopyIssueSummary()
        {
            Clipboard.SetText(String.Format("{0}: {1}", this._issue.Key.Value, this._issue.Summary));
        }

        private void CopyIssueKey()
        {
            Clipboard.SetText(this._issue.Key.Value);
        }

        private void OpenIssueToolWindow()
        {
            this._services.VisualStudioServices.ShowJiraIssueToolWindow(this, this._services);
        }

        private void OpenIssue()
        {
            var baseUri = new Uri(this._issue.Jira.Url);
            var issueUrl = new Uri(baseUri, "browse/" + this._issue.Key.Value);
            System.Diagnostics.Process.Start(issueUrl.ToString());
        }
    }
}
