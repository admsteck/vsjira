﻿using Atlassian.Jira;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Input;

namespace VSJira.Core.UI
{
    public class JiraIssuesPagerViewModel : JiraBackedViewModel
    {
        public class ColumnHeaderChangeEventArgs : EventArgs
        {
            public string Field { get; set; }
            public ListSortDirection? SortDirection { get; set; }
        }

        private string _jql;
        private int _itemsPerPage;
        private int _totalItems;
        private int _currentItemIndex;
        private string _sortField;
        private ListSortDirection? _sortDirection;

        private CancellationTokenSource _cancellationSource;
        public event EventHandler<ColumnHeaderChangeEventArgs> ColumnHeaderChanged;
        public ObservableCollection<JiraIssueViewModel> Issues { get; private set; }
        public DelegateCommand FirstCommand { get; private set; }
        public DelegateCommand PreviousCommand { get; private set; }
        public DelegateCommand NextCommand { get; private set; }
        public DelegateCommand LastCommand { get; private set; }

        public JiraIssuesPagerViewModel(VSJiraServices services)
            : base(services)
        {
            this.Issues = new ObservableCollection<JiraIssueViewModel>();
            this._cancellationSource = new CancellationTokenSource();

            InitializeCommands();
        }

        public int ItemsPerPage
        {
            get { return _itemsPerPage; }
        }

        public int TotalItems
        {
            get { return _totalItems; }
        }

        public int Start
        {
            get { return _currentItemIndex + 1; }
        }

        public int End
        {
            get { return _currentItemIndex + _itemsPerPage < _totalItems ? _currentItemIndex + _itemsPerPage : _totalItems; }
        }

        public async Task LoadIssuesAsync(string jql, int maxIssuesPerRequest)
        {
            this._jql = jql;
            this._currentItemIndex = 0;
            this._itemsPerPage = maxIssuesPerRequest;

            // Clear the sorting UI
            OnColumnHeaderChanged(this._sortField, null);
            this._sortField = null;
            this._sortDirection = null;

            await this.RefreshIssuesAsync();
        }

        public async Task SortIssuesAsync(string field)
        {
            if (!field.Equals(this._sortField, StringComparison.OrdinalIgnoreCase))
            {
                this.OnColumnHeaderChanged(this._sortField, null);
                this._sortDirection = ListSortDirection.Ascending;
                this._sortField = field;
            }
            else
            {
                this._sortDirection = this._sortDirection != ListSortDirection.Ascending ?
                    ListSortDirection.Ascending : ListSortDirection.Descending;
            }

            this._currentItemIndex = 0;
            this.OnColumnHeaderChanged(this._sortField, this._sortDirection);

            await this.RefreshIssuesAsync();
        }

        public void CancelOperation()
        {
            this._cancellationSource.Cancel();
            this.Issues.Clear();
        }

        private string PrepareJql(string jql)
        {
            if (this._sortDirection == null)
            {
                return jql;
            }

            var orderByIndex = jql.IndexOf("Order by", StringComparison.OrdinalIgnoreCase);

            if (orderByIndex > 0)
            {
                jql = jql.Substring(0, orderByIndex);
            }

            return String.Format("{0} Order by {1} {2}",
                jql,
                this._sortField,
                this._sortDirection == ListSortDirection.Ascending ? "asc" : "desc");
        }

        private async Task RefreshIssuesAsync()
        {
            this._cancellationSource.Cancel();
            this._cancellationSource = new CancellationTokenSource();
            this.Issues.Clear();
            var jql = PrepareJql(this._jql);

            // refresh UI of expected results before issuing the request.
            UpdatePageCounts();

            IPagedQueryResult<Issue> pagedResult = null;

            try
            {
                pagedResult = await this.Jira.RestClient.GetIssuesFromJqlAsync(jql, this._itemsPerPage, this._currentItemIndex, _cancellationSource.Token) as IPagedQueryResult<Issue>;
            }
            catch (OperationCanceledException)
            {
                // No-op.
            }
            catch (Exception ex)
            {
                this._services.WindowFactory.ShowMessageBox("There was an error retrieving the issues from server. Details: " + ex.Message);
            }

            if (pagedResult != null)
            {
                this._totalItems = pagedResult.TotalItems;
                foreach (var issue in pagedResult)
                {
                    this.Issues.Add(new JiraIssueViewModel(issue, this._services));
                }

                UpdatePageCounts();
            }
        }

        private void UpdatePageCounts()
        {
            OnPropertyChanged("TotalItems");
            OnPropertyChanged("Start");
            OnPropertyChanged("End");

            CommandManager.InvalidateRequerySuggested();
        }

        private void OnColumnHeaderChanged(string field, ListSortDirection? sortDirection)
        {
            var args = new ColumnHeaderChangeEventArgs()
            {
                Field = field,
                SortDirection = sortDirection
            };

            var handler = this.ColumnHeaderChanged;
            if (handler != null)
            {
                handler(this, args);
            }
        }

        private void InitializeCommands()
        {
            this.FirstCommand = new DelegateCommand(
                async () =>
                {
                    _currentItemIndex = 0;
                    await this.RefreshIssuesAsync();
                },
                () => _currentItemIndex - _itemsPerPage >= 0 ? true : false);

            this.PreviousCommand = new DelegateCommand(
                async () =>
                {
                    _currentItemIndex -= _itemsPerPage;
                    await this.RefreshIssuesAsync();
                },
                () => _currentItemIndex - _itemsPerPage >= 0 ? true : false);

            this.NextCommand = new DelegateCommand(
                async () =>
                {
                    _currentItemIndex += _itemsPerPage;
                    await this.RefreshIssuesAsync();
                },
                () => _currentItemIndex + _itemsPerPage < _totalItems ? true : false);

            this.LastCommand = new DelegateCommand(
                async () =>
                {
                    _currentItemIndex = (_totalItems / _itemsPerPage - 1) * _itemsPerPage;
                    _currentItemIndex += _totalItems % _itemsPerPage == 0 ? 0 : _itemsPerPage;
                    await this.RefreshIssuesAsync();
                },
                () => _currentItemIndex + _itemsPerPage < _totalItems ? true : false);
        }
    }
}
