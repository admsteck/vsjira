﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;

namespace VSJira.Core.UI
{
    public class WindowFactory : IWindowFactory
    {
        private readonly Dictionary<Type, Func<object, Window>> _factoryMap = new Dictionary<Type, Func<object, Window>>()
        {
            { typeof(ConnectDialogViewModel), (vm) => new ConnectDialog((ConnectDialogViewModel)vm) }
        };

        private Window CreateWindowForViewModel(object viewModel)
        {
            var window = _factoryMap[viewModel.GetType()].Invoke(viewModel);
            window.DataContext = viewModel;
            return window;
        }

        public void Show(object viewModel)
        {
            CreateWindowForViewModel(viewModel).Show();
        }

        public void ShowDialog(object viewModel)
        {
            CreateWindowForViewModel(viewModel).ShowDialog();
        }

        public void ShowMessageBox(string text)
        {
            MessageBox.Show(text);
        }
    }
}
