﻿using Atlassian.Jira;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace VSJira.Core.UI
{
    public class ConnectDialogViewModel : ViewModelBase
    {
        public class RequestPasswordEventArgs : EventArgs
        {
            public string Password { get; set; }
        }

        private readonly IJiraFactory _jiraFactory;
        private readonly VSJiraServices _services;

        private string _url;
        private string _username;
        private ConnectDialogStatus _status;
        private JiraServerInfo _selectedServer;
        private bool _isUrlFocused;
        private bool _isPasswordFocused;

        public Jira Jira { get; private set; }
        public ObservableCollection<JiraServerInfo> Servers { get; private set; }
        public event EventHandler<RequestPasswordEventArgs> RequestingPassword;
        public DelegateCommand<ICloseableWindow> ConnectCommand { get; private set; }
        public DelegateCommand<ICloseableWindow> ShowOptionsPageCommand { get; private set; }

        public ConnectDialogViewModel(IJiraFactory jiraFactory, VSJiraServices services)
        {
            this._jiraFactory = jiraFactory;
            this._services = services;

            this.Status = ConnectDialogStatus.Idle;
            this.ConnectCommand = new DelegateCommand<ICloseableWindow>(async (window) => await this.ConnectToJira(window));
            this.ShowOptionsPageCommand = new DelegateCommand<ICloseableWindow>((window) => this.ShowOptionsPage(window));
            this.Servers = new ObservableCollection<JiraServerInfo>(this._services.VisualStudioServices.GetConfigurationOptions().JiraServers);
            this.SelectedServer = this.Servers.FirstOrDefault();
        }

        public bool IsUrlFocused
        {
            get { return _isUrlFocused; }
            set
            {
                _isUrlFocused = value;
                OnPropertyChanged("IsUrlFocused");
            }
        }

        public bool IsPasswordFocused
        {
            get { return _isPasswordFocused; }
            set
            {
                _isPasswordFocused = value;
                OnPropertyChanged("IsPasswordFocused");
            }
        }

        public JiraServerInfo SelectedServer
        {
            get { return _selectedServer; }
            set
            {
                _selectedServer = value;
                OnPropertyChanged("SelectedServer");
                PopulateFieldsFromSettings();

                this.IsUrlFocused = false;
                this.IsPasswordFocused = false;

                if (_selectedServer == null)
                {
                    this.IsUrlFocused = true;
                }
                else
                {
                    this.IsPasswordFocused = true;
                }
            }
        }

        public ConnectDialogStatus Status
        {
            get { return _status; }
            set
            {
                _status = value;
                OnPropertyChanged("Status");
            }
        }

        public string Url
        {
            get { return _url; }
            set
            {
                _url = value.Trim();
                OnPropertyChanged("Url");
            }
        }

        public string Username
        {
            get { return _username; }
            set
            {
                _username = value.Trim();
                OnPropertyChanged("Username");
            }
        }

        private void PopulateFieldsFromSettings()
        {
            if (this.SelectedServer != null)
            {
                this.Url = this.SelectedServer.Url;
                this.Username = this.SelectedServer.Username;
            }
        }

        private Jira CreateJira()
        {
            Jira jira = null;

            try
            {
                var passwordArgs = new RequestPasswordEventArgs();
                this.OnRequestPassword(passwordArgs);
                jira = this._jiraFactory.Create(
                    this.Url,
                    this.Username,
                    passwordArgs.Password);
            }
            catch
            {
                this.Status = ConnectDialogStatus.ValidationError;
            }

            return jira;
        }

        private Task PrepareJiraClient(Jira jira)
        {
            return Task.WhenAll(
                jira.RestClient.GetCustomFieldsAsync(CancellationToken.None),
                jira.GetIssuePrioritiesAsync(CancellationToken.None),
                jira.GetIssueStatusesAsync(CancellationToken.None),
                jira.GetIssueTypesAsync(CancellationToken.None),
                jira.GetIssueResolutionsAsync(CancellationToken.None)
           );
        }

        private async Task ConnectToJira(ICloseableWindow window)
        {
            this.Status = ConnectDialogStatus.Busy;

            var jira = CreateJira();

            if (jira != null)
            {
                try
                {
                    await PrepareJiraClient(jira);
                    this.Jira = jira;
                    window.Close();
                }
                catch
                {
                    this.Status = ConnectDialogStatus.ConnectionError;
                }
            }
        }

        private void ShowOptionsPage(ICloseableWindow window)
        {
            window.Close();
            this._services.VisualStudioServices.ShowOptionsPage();
        }

        /// <summary>
        /// This is a work around to get the password from view given that the
        /// PasswordBox is not bindable
        /// </summary>
        private void OnRequestPassword(RequestPasswordEventArgs args)
        {
            var handler = this.RequestingPassword;
            if (handler != null)
            {
                handler(this, args);
            }
        }
    }
}
