﻿using Atlassian.Jira;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace VSJira.Core.UI
{
    public class JiraIssueUserControlViewModel : ViewModelBase
    {
        private JiraIssueViewModel _originalIssue;
        private JiraIssueViewModel _editableIssue;
        private bool _isIdle = true;

        public VSJiraServices Services { get; set; }
        public ObservableCollection<string> Priorities { get; private set; }
        public ObservableCollection<string> Statuses { get; private set; }
        public ObservableCollection<string> Resolutions { get; private set; }
        public ObservableCollection<string> Types { get; private set; }
        public DelegateCommand SaveIssueCommand { get; private set; }
        public DelegateCommand RefreshIssueCommand { get; private set; }

        public JiraIssueUserControlViewModel()
        {
            this.Priorities = new ObservableCollection<string>();
            this.Statuses = new ObservableCollection<string>();
            this.Resolutions = new ObservableCollection<string>();
            this.Types = new ObservableCollection<string>();

            this.RefreshIssueCommand = new DelegateCommand(() => RefreshIssueAsync());

            this.SaveIssueCommand = new DelegateCommand(
                async () => await SaveIssueAsync(),
                () => this.IsIdle && this.Issue != null);
        }

        public JiraIssueViewModel Issue
        {
            get { return _editableIssue; }
            set
            {
                _originalIssue = value;
                _editableIssue = _originalIssue.Clone();
                OnPropertyChanged("Issue");
                this.LoadComboBoxes();
            }
        }

        public bool IsIdle
        {
            get { return _isIdle; }
            set
            {
                _isIdle = value;
                OnPropertyChanged("IsIdle");
            }
        }

        private async Task RefreshIssueAsync()
        {
            this.IsIdle = false;
            try
            {
                await this._originalIssue.RefreshIssueAsync(CancellationToken.None);
                this._editableIssue = _originalIssue.Clone();
                OnPropertyChanged("Issue");
            }
            catch (OperationCanceledException)
            {
                // No-op.
            }
            catch (Exception ex)
            {
                this.Services.WindowFactory.ShowMessageBox("There was an error retrieving the issue from server. Details: " + ex.Message);
            }

            this.IsIdle = true;
        }

        private async Task SaveIssueAsync()
        {
            this.IsIdle = false;
            try
            {
                await this._originalIssue.UpdateIssueAsync(this._editableIssue, CancellationToken.None);
            }
            catch (OperationCanceledException)
            {
                // No-op.
            }
            catch (Exception ex)
            {
                this.Services.WindowFactory.ShowMessageBox("There was an error updating the issue. Details: " + ex.Message);
            }

            this.IsIdle = true;
        }

        private void LoadComboBoxes()
        {
            if (this._editableIssue == null)
            {
                return;
            }

            var jira = this._editableIssue.Jira;
            foreach (var priority in jira.GetIssuePriorities())
            {
                this.Priorities.Add(priority.Name);
            }

            foreach (var type in jira.GetIssueTypes())
            {
                this.Types.Add(type.Name);
            }

            foreach (var status in jira.GetIssueStatuses())
            {
                this.Statuses.Add(status.Name);
            }

            foreach (var resolution in jira.GetIssueResolutions())
            {
                this.Resolutions.Add(resolution.Name);
            }
        }
    }
}
