﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using VSJira.Core.UI;

namespace VSJira.Core
{
    public interface IVisualStudioServices
    {
        IJiraOptions GetConfigurationOptions();
        void ShowOptionsPage();
        void ShowJiraIssueToolWindow(JiraIssueViewModel viewModel, VSJiraServices services);
    }
}
