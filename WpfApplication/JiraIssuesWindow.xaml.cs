﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using VSJira.Core;
using VSJira.Core.UI;

namespace WpfApplication
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
            var services = new VSJiraServices()
            {
                JiraFactory = new TestJiraFactory(),
                WindowFactory = new WindowFactory(),
                VisualStudioServices = new MockVisualStudioServices()
            };
            var vm = new JiraIssuesUserControlViewModel(services);

            this.JiraIssuesControl.Content = new JiraIssuesUserControl(vm);
        }
    }
}
