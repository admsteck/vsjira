﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using VSJira.Core;

namespace WpfApplication
{
    public class MockVisualStudioServices : IVisualStudioServices
    {
        public IJiraOptions GetConfigurationOptions()
        {
            return new JiraOptions()
            {
                MaxIssuesPerRequest = 100,
                JiraServers = new JiraServerInfo[2]
                {
                    new JiraServerInfo("Live", "http://104.42.227.211:8080/", "admin"),
                    new JiraServerInfo("Test2", "http://foo.bar2", "myuser2"),
                }
            };
        }

        public void ShowOptionsPage()
        {
            MessageBox.Show("Show Options Page");
        }

        public void ShowJiraIssueToolWindow(VSJira.Core.UI.JiraIssueViewModel viewModel, VSJiraServices services)
        {
            var window = new JiraIssueWindow(viewModel, services);
            window.Show();
        }
    }
}
